<!DOCTYPE html>
<html lang="en">
<head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
        <script src="public/js/jquery.min.js"></script>
        <script src="public/js/moment.min.js"></script>
        <link rel="stylesheet" href="public/css/fullcalendar.min.css">
        <script src="public/js/fullcalendar.min.js"></script>
        <script src="public/js/calendario.js"></script>
        <script src="public/js/es.js"></script>
        <script src="public/js/bootstrap-clockpicker.js"></script>
        <link rel="stylesheet" href="public/css/bootstrap-clockpicker.css">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
        <title>Documnet</title>


</head>
<body>

<?php require 'views/header.php'; ?>


<div id="main">
<h1 class="center">Agenda Web</h1>
<!--div donde se inserta el calendario -->
<form action="<?php  echo constant('URL'); ?>consultar.php" method="POST">

<div class="container">
        <div class="row">
        <div class="col"></div>
        <div class="col-16"> <div id="calendarioweb"> </div></div>
        <div class="col"></div>
        </div>
</div>
</form>
<!-- fin div donde se inserta el calendario -->
</div><br><br>
<?php require 'views/footer.php'; ?>

<!--modal del calendario para modificar datos -->
<div class="modal fade" id="ModalEventos" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="tituloEvento">Actividad para hoy</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        
        <input type="hidden" id="txtID" name="txtID">
        <input type="hidden" id="txtFecha" name="txtFecha"/>
        
        <div class="form-row">
            <div class="form-group col-md-8">
                  <label>Titulo:</label>
                 <input type="text" id="txtTitulo" class="form-control" placeholder="Titulo del evento">
           </div>

           <div class="form-group col-md-4">
                  <label>Hora del evento:</label>

                  <div class="input-group clockpicker" data-autoclose="true">
                 <input type="text" id="txtHora" class="form-control" placeholder="Hora"/>
                 </div>
           </div>
           </div>
           <div class="form-group">
                  <label>Descripcion:</label>
                 <input type="text" id="txtDescripcion" class="form-control" placeholder="Descripcion">
           </div>
           <div class="form-group">
                  <label>color:</label>
                 <input type="color" id="txtColor" class="form-control" placeholder="color" style="height:36px">
           </div>
          

      </div>
      <div class="modal-footer">
        <button type="button" id="btnAgregar" class="btn btn-primary">Agregar</button>
        <button type="button" id="btnModificar" class="btn btn-success">Modificar</button>
        <button type="button" id="btnEliminar" class="btn btn-danger">Borrar</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
      </div>
    </div>
  </div>
</div>
<!-- fin del modal del calendario para modificar los datos -->
</body>
</html>