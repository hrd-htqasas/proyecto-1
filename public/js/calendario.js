
//funcionq que muestra el emcabezado del calendario
 $(document).ready(function(){
 $('#calendarioweb').fullCalendar({
    header:{
        left:'today, prev, next',
        center:'title',
        right:'month,basicWeek,basicDay,agendaWeek,agendaDay'
    },
    // fin de lafuncion que muestra el emcabezado del calendario
    dayClick:function(date,jsEvent,view){
        
        $('btnAgregar').prop("disable", false);
        $('btnModificar').prop("disable", true);
        $('btnEliminar').prop("disable", true);
        $('#txtFecha').val(date.format());
        $("#ModalEventos").modal();
    },
    // cargar el json
     events:'http://localhost/PROYECTOS/mvc/models/consultar.php', 

     // fin de lafuncion que mue
    eventClick:function(calEvent,jsEvent,view){
        $('btnAgregar').prop("disable", true);
        $('btnModificar').prop("disable", false);
        $('btnEliminar').prop("disable", false);

        $('#tituloEvento').html(calEvent.title);
        $('#txtDescripcion').val(calEvent.descripcion);
        $('txtID').val(calEvent.id);
        $('txtTitulo').val(calEvent.title);
        $('textColor').val(calEvent.color);

        FechaHora= calEvent.start._i.split(" ");
        $('txtFecha').val(FechaHora[0]);

        $("#ModalEventos").modal();
    },
    editable:true,
    eventDrop:function(calEvent){
        $('txtID').val(calEvent.id); 
        $('txtTitulo').val(calEvent.title);
        $('textColor').val(calEvent.color); 
        $('#txtDescripcion').val(calEvent.descripcion);

        var FechaHora=calEvent.start.format().split("T");
        $('txtFecha').val(FechaHora[0]);
        $('txtFecha').val(FechaHora[1]);
        ConDatos();
        EnviarDatos('modificar',NuevoEvento,true);

            }
       });

       var NuevoEvento;
$('#btnAgregar').click(function(){
    ConDatos();
    EnviarDatos('agregar',NuevoEvento);
         
});
 

$('#btnEliminar').click(function(){
    ConDatos();
    EnviarDatos('eliminar' ,NuevoEvento);

      
});

$('#btnModificar').click(function(){
    ConDatos();
    EnviarDatos('modificar' ,NuevoEvento);

      
});

function ConDatos(){
    var NuevoEvento= {
        id:$('#txtID').val(),
        title:$('#txtTitulo').val(),
        start:$('#txtFecha').val()+" "+$('#txtHora').val(),  
        color:$('#textColor').val(),
        descripcion:$('#txtDescripcion').val(),
        textColor:"#4C191D", 
        end:$('#txtFecha').val()+" "+$('#txtHora').val()
    };
}
function EnviarDatos(accion,objEvento,modal){
    $.ajax({
        type:'POST',
        url:'consultar.php?accion='+accion,
        data:objEvento,
        success:function(msg){
            if(msg){
                $('#calendarioweb').fullCalendar('refetchEvents');
                if(!modal){
                    $("#ModalEventos").modal('toggle');

                }
            }
        },
        error:function(){
            alert("Error en la operacion");
 
        }
    
    
    });
}

$('.clockpicker').clockpicker();
    function Limpiar(){
        $('txtID').val(''); 
        $('txtTitulo').val('');
        $('txtColor').val(''); 
        $('#txtDescripcion').val('');
    }
    
    });

